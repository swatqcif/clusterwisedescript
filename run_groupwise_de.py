#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 16 15:51:54 2022

@author: s2992547

"""

import pandas as pd
import scanpy as sc
import argparse
import sys


#adata_file = "data/Clusters_AnnData_nonNeg.h5ad"

#subsets_to_test_within_column =  "louvain"
#subsets_to_test_within        = adata.obs[subsets_to_test_within_column].unique()
#contrast_group_column = "genotype"
#condition = "knockout"
#reference = "wildtype"

#min_cells_per_group = 50
#output_file = "rank_genes_groups_clusterwise_de.tab"


# ./run_groupwise_de.py  data/Clusters_AnnData_nonNeg.h5ad louvain genotype knockout wildtype output.tab

parser = argparse.ArgumentParser(description='Simple wrapper to run rank_genes_groups to highlight '+
                                 'expression differences between groups among different clusters. '+
                                 'Takes the simplest possible approach of carving up experiment a group at a time '+
                                 "then running scanpy scanpy.tl.rank_genes_groups for differential experssion. " +
                                 "More sensitive and sophisticated methods exist! ")

parser.add_argument('anndata', metavar="AnnDataFile", 
                    help=' .h5ad AnnData file')

parser.add_argument('split_column', metavar='split_column',
                    help='Column in anndata to subset by, before testing for differences. Typically the cluster column (e.g. louvain)')

parser.add_argument('contrast_group_column', metavar="contrast_group_column", 
                    help='Column in anndata which describes the groups being compared. Typically experimental condition (e.g. genotype)')


parser.add_argument('condition', metavar="condition", 
                    help='Value in the contrast group column that indicates the test group for comparisons (e.g. knockout) '+
                    'Comparisons will be (condition - reference) ')

parser.add_argument('reference', metavar="reference", 
                    help='Value in the contrast group column that indicates the reference group for comparisons (e.g. control) '+
                    'Comparisons will be (condition - reference) ')

parser.add_argument('output_file', metavar="output_file", 
                    help='File to write differential expression table')

# optional
parser.add_argument('-c', '--min_cells_per_group', metavar="min_cells_per_group", 
                    default=50, type = int,
                    help='Require at least this many cells in a group to perform a comparison for a group. Applies to both condition and reference groups.')

# Use raw (consistancy with other methods)
parser.add_argument('-r', '--useraw', 
                    action='store_true',
                    help='Use raw attribute in anndata object, see scanpy documentation for scanpy.tl.rank_genes_groups. ')


#Use tie correction for 'wilcoxon' scores. Used only for 'wilcoxon'.
# seems unncesseary

#Limiting number of returned genes (-n)
# seems uneccessary


# optional
# Log reg hits iteration issues.
#/Applications/miniconda3/envs/pyscrna/lib/python3.8/site-packages/sklearn/linear_model/_logistic.py:814: ConvergenceWarning: lbfgs failed to converge (status=1):
#STOP: TOTAL NO. of ITERATIONS REACHED LIMIT.
#
#Increase the number of iterations (max_iter) or scale the data as shown in:
#    https://scikit-learn.org/stable/modules/preprocessing.html
#Please also refer to the documentation for alternative solver options:
#    https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression
#  n_iter_i = _check_optimize_result(
parser.add_argument('-m', '--method', metavar="method", 
                    default='t-test',
                    
                    choices=[ 't-test', 'wilcoxon', 't-test_overestim_var'],
                    help='Test method to use, see scanpy documentation for scanpy.tl.rank_genes_groups.' +
                    "options are 't-test' (default),  'wilcoxon', 't-test_overestim_var'. " +
                    "logreg not currently supported.")



args = parser.parse_args()




#%% scanpy scripts functions

# These functions taken from scanpy_scrips - want consistancy with that output
# https://github.com/ebi-gene-expression-group/scanpy-scripts/blob/0f44c7db6be9422819cba9560b0bf3b18838bd7f/scanpy_scripts/lib/_diffexp.py#L104
def extract_de_table(de_dict):
    """
    Extract DE table from adata.uns
    """
    if de_dict['params']['method'] == 'logreg':
        requested_fields = ('scores',)
    else:
        requested_fields = ('scores', 'logfoldchanges', 'pvals', 'pvals_adj',)
    gene_df = _recarray_to_dataframe(de_dict['names'], 'genes')[
        ['cluster', 'rank', 'genes']]
    gene_df['ref'] = de_dict['params']['reference']
    gene_df = gene_df[['cluster', 'ref', 'rank', 'genes']]
    de_df = pd.DataFrame({
        field: _recarray_to_dataframe(de_dict[field], field)[field]
        for field in requested_fields if field in de_dict
    })
    return gene_df.merge(de_df, left_index=True, right_index=True)


# From scanpy scrips
def _recarray_to_dataframe(array, field_name):
    return pd.DataFrame(array).reset_index().rename(
        columns={'index': 'rank'}).melt(
            id_vars='rank', var_name='cluster', value_name=field_name)
            


            
#%% script functions

def get_testable_subsets (
        subsets_to_test_within_column,         
        contrast_group_column,          
        condition,                      
        reference,
        min_cells_per_group) :
    
    all_subsets         = adata.obs[subsets_to_test_within_column].cat.categories
    ok_subsets = []
    
    for the_subset in all_subsets :
    
    
        adata_subset = adata[adata.obs[subsets_to_test_within_column] == the_subset]
        
        contrast_vals = adata_subset.obs[contrast_group_column]
    
    
        if ( sum(contrast_vals == condition ) >= min_cells_per_group and 
             sum(contrast_vals == reference ) >= min_cells_per_group ) :
            ok_subsets.append(the_subset)
    
    return(ok_subsets)



def run_rank_genes_groups_on_subsets (       
        subsets_to_test_within_column,         
        contrast_group_column,          
        condition,                      
        reference,
        subsets,
        method,
        use_raw
    ) :
    
    result_list = []
    
    # <Insert parallelisation here >
    for the_subset in subsets :
        
        result = run_rank_genes_groups_on_subset (
                subsets_to_test_within_column,  
                the_subset,                     
                contrast_group_column,          
                condition,                      
                reference,
                method,
                use_raw) 

        result_list.append(result)

    
    results_all = pd.concat(result_list)
    
    return(results_all)



def run_rank_genes_groups_on_subset (
        subsets_to_test_within_column,  # e.g louvain
        the_subset,                     # e.g. [cluster] '3'
        contrast_group_column,          # Column name e.g. genotype.
        condition,                      # test level in contrastss colum e.g. 'knockout'
        reference,                      # reference level in contrasts column e.g. 'control
        method,                          # method, see sc.tl.rank_genes_groups
        use_raw                        
    ) :
    
    # Name for the contrast results
    diff_key = "_".join(['rank_genes_groups',
                        contrast_group_column,
                        subsets_to_test_within_column, 
                        the_subset ])

    adata_subset = adata[adata.obs[subsets_to_test_within_column] == the_subset]


    # https://scanpy.readthedocs.io/en/stable/generated/scanpy.tl.rank_genes_groups.html
    # Seems to be no explicit parallelisation here. (why not? its paralllelisable!)
    # We could parallise on subsets.
    sc.tl.rank_genes_groups(
        adata_subset, 
        use_raw     = use_raw, 
        rankby_abs  = True,    # Only true approriate for this kind of DE.
        #n_genes    = n_genes, # easy post filter.
        method      = method,  # t-test wilcoxon t-test_overestim_var  Logreg runs out of iterations, out of the box.
        key_added   = diff_key, 
        reference   = reference,
        groupby     = contrast_group_column, 
        groups      = [condition] )


    # ALmost the same as scanpy-scripts otuput...
    result = extract_de_table(adata_subset.uns[diff_key])
    # but need to add an extra column with what the subset is.
    result.insert(0, "diff_test", value=diff_key)
    
    return(result)



#%%  GO

# Load it
adata = sc.read_h5ad(args.anndata)


# Use raw is a common option, but doesn't make sense if theres no .raw data inside.
if (args.useraw and adata.raw == None)  :
    sys.exit("--useraw option supplied but no .raw data in anndata object")


# given half-helpful errors for column name typos.   
if ( not args.split_column in adata.obs.columns ) :
    sys.exit("Can't find column '"+args.split_column+"' anndata object")

if ( not args.contrast_group_column in adata.obs.columns ) :
    sys.exit("Can't find column '"+args.contrast_group_column+"' anndata object")


# or the condition and control values 
if (not args.condition in adata.obs[args.contrast_group_column].cat.categories) :
    sys.exit("Can't find condition value '"+
             args.condition+"' in contrast column '"+
             args.contrast_group_column+"' of anndata object")

if (not args.reference in adata.obs[args.contrast_group_column].cat.categories) :
    sys.exit("Can't find reference value '"+
             args.reference+"' in contrast column '"+
             args.contrast_group_column+"' of anndata object")



# What subsets/clusters are testable?
testable_subsets = get_testable_subsets(        
        subsets_to_test_within_column = args.split_column,         
        contrast_group_column         = args.contrast_group_column,          
        condition                     = args.condition,                      
        reference                     = args.reference,  
        min_cells_per_group           = args.min_cells_per_group)


# ALso overzelous clustering with tiny groups, or overcondience in cell counts!
if (len(testable_subsets) == 0 ) : 
    sys.exit("No groups (with at least "+ str(args.min_cells_per_group) +" cells on each side of comparison) to test!")


# Run the DE for all the contrasts and join results?
results_all = run_rank_genes_groups_on_subsets (       
        subsets_to_test_within_column = args.split_column,         
        contrast_group_column         = args.contrast_group_column,          
        condition                     = args.condition,                      
        reference                     = args.reference,  
        subsets                       = testable_subsets,
        method                        = args.method,
        use_raw                       = args.useraw
        )

# save it
results_all.to_csv(args.output_file, sep="\t", index=False)



