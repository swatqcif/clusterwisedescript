# -*- coding: utf-8 -*-

#import numpy as np
#import pandas as pd
#import scanpy as sc
#import numpy as np


#%%  Load

# https://zenodo.org/record/4624461/files/Mito-counted_AnnData
#adata = sc.read_h5ad("data/Mito-counted_AnnData.h5ad")


# output of tutorial: https://training.galaxyproject.org/training-material/topics/transcriptomics/tutorials/scrna-seq-basic-pipeline/tutorial.html?utm_source=smorgasbord&utm_medium=website&utm_campaign=gcc2021
#adata = sc.read_h5ad("data/Clusters_AnnData.h5ad") # with negatieves

#adata = sc.read_h5ad("data/Clusters_AnnData_nonNeg.h5ad")

# adda raw object.
#adata.raw = adata
#adata.write("data/Clusters_AnnData_nonNeg_withraw.h5ad")

## DODGEY HACK
## add 6.7 to all non zero values, leave the 0s alone. 
#adata.X = np.where(adata.X != 0, adata.X + 6.7, 0 )
#adata.write("data/Clusters_AnnData_nonNeg.h5ad")

# restructure workflow so this isn't an issue.

# Rescale to avoid negative values
# https://github.com/theislab/scanpy/issues/653

