# run_groupwise_de.py


Simple wrapper script for scanpy.tl.rank_genes_groups. Splits up a anndata object by [cluster] column, then tests on [experimental condition column] for experiment condition - reference condition. Uses code from scanpy-scripts for nice output format.

